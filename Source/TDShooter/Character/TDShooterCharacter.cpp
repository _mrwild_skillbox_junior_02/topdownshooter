// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"

ATDShooterCharacter::ATDShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	// Character rotation on mouse cursor pointer
	FollowMouseCursorCharacterRotation(DeltaSeconds);

	// Stamina logic functions call
	StaminaLogic(DeltaSeconds);
}

// CHARACTER INPUT BINDINGS
void ATDShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (PlayerInputComponent)
	{
		Super::SetupPlayerInputComponent(PlayerInputComponent);

		PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDShooterCharacter::InputAxisX);
		PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDShooterCharacter::InputAxisY);

		PlayerInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &ATDShooterCharacter::InputAimPressed);
		PlayerInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &ATDShooterCharacter::InputAimReleased);

		PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &ATDShooterCharacter::InputWalkPressed);
		PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &ATDShooterCharacter::InputWalkReleased);

		PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATDShooterCharacter::InputSprintPressed);
		PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATDShooterCharacter::InputSprintReleased);
	}
}

// MOVEMENT TICK-LOGIC DEFINITION
void ATDShooterCharacter::FollowMouseCursorCharacterRotation(float DeltaTime)
{
	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult CursorHitResult;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, CursorHitResult);
		
		FRotator ActorFollowCursorRotation(ForceInitToZero);
		ActorFollowCursorRotation.Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorHitResult.Location).Yaw;
		SetActorRotation(ActorFollowCursorRotation);
	}
}

// MOVEMENT STATE EVENTS LOGIC
void ATDShooterCharacter::CharacterUpdate()
{
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.AimSpeed;
		break;
	case EMovementState::Walk_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.WalkSpeed;
		break;
	case EMovementState::Run_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.RunSpeed;
		break;
	case EMovementState::AimWalk_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.AimWalkSpeed;
		break;
	case EMovementState::SprintRun_State:
		GetCharacterMovement()->MaxWalkSpeed = MovementSpeed.SprintRunSpeed;
		break;
	default:
		break;
	}
}

void ATDShooterCharacter::ChangeMovementState()
{
	if (!bSprintEnabled && !bWalkEnabled && !bAimEnabled)
		MovementState = EMovementState::Run_State;
	else if (bSprintEnabled)
		MovementState = EMovementState::SprintRun_State;
	else if (!bSprintEnabled && bWalkEnabled && bAimEnabled)
		MovementState = EMovementState::AimWalk_State;
	else if (!bSprintEnabled && bWalkEnabled && !bAimEnabled)
		MovementState = EMovementState::Walk_State;
	else if (!bSprintEnabled && !bWalkEnabled && bAimEnabled)
		MovementState = EMovementState::Aim_State;

	CharacterUpdate();
}

// INPUT FUNCTIONS DEFINITION
void ATDShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
	bIsIdleOrBackwards = false;
	AddMovementInput(FVector(GetActorForwardVector()), AxisX);

	if (AxisX == 0 || AxisX < 0)
		bIsIdleOrBackwards = true;
}

void ATDShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
	if (!bSprintEnabled && AxisY != 0)
	{
		bIsStrafing = true;
		AddMovementInput(FVector(GetActorRightVector()), AxisY);
	}
	else
		bIsStrafing = false;
}

void ATDShooterCharacter::InputAimPressed()
{
	if (!bSprintEnabled)
	{
		bAimEnabled = true;
		ChangeMovementState();
	}
}

void ATDShooterCharacter::InputAimReleased()
{
	bAimEnabled = false;
	ChangeMovementState();
}

void ATDShooterCharacter::InputWalkPressed()
{
	if (!bSprintEnabled)
	{
		bWalkEnabled = true;
		ChangeMovementState();
	}
}

void ATDShooterCharacter::InputWalkReleased()
{
	bWalkEnabled = false;
	ChangeMovementState();
}

void ATDShooterCharacter::InputSprintPressed()
{
	if (bSprintAllowed && !bIsStrafing && !bIsIdleOrBackwards)
	{
		bSprintEnabled = true;
		bAimEnabled = false;
		ChangeMovementState();
	}
}

void ATDShooterCharacter::InputSprintReleased()
{
	bSprintEnabled = false;
	ChangeMovementState();
}


void ATDShooterCharacter::StaminaLogic(float DeltaSeconds)
{
	if (!bSprintEnabled && Stamina <= 100.f)
		StaminaRestore(DeltaSeconds);

	else if (bSprintEnabled)
		SprintRunOn(DeltaSeconds);

	Stamina = FMath::Clamp<float>(Stamina, 0.f, 100.f);
}

void ATDShooterCharacter::SprintRunOn(float DeltaSeconds)
{
	Stamina -= StaminaUsageRate * DeltaSeconds;

	if (Stamina <= 0)
	{
			bSprintEnabled = false;
			bSprintAllowed = false;
			ChangeMovementState();
	}
}

void ATDShooterCharacter::StaminaRestore(float DeltaSeconds)
{
	Stamina += StaminaRestoreRate * DeltaSeconds;

	if (Stamina > SprintAllowedStamina)
		bSprintAllowed = true;
}
