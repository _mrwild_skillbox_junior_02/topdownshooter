// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDShooter/FunctionLib/Types.h"
#include "TDShooterCharacter.generated.h"


UCLASS(Blueprintable)
class ATDShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	// UPROPERTIES Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeed;

	UPROPERTY(BlueprintReadWrite, Category = "MovementType")
		bool bAimEnabled = false;

	UPROPERTY(BlueprintReadWrite, Category = "MovementType")
		bool bSprintEnabled = false;

	UPROPERTY(BlueprintReadWrite, Category = "MovementType")
		bool bWalkEnabled = false;

	// UPROPERTIES Stamina
	UPROPERTY(BlueprintReadWrite, Category = "Stamina")
		float Stamina = 100.f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
		float StaminaUsageRate = 40.f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
		float StaminaRestoreRate = 10.f;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
		int SprintAllowedStamina = 30;

	// UFUNCTIONS
	// These functions may be deleted after input implementation due to code-based logic
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	// Input functions
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAimPressed();
	UFUNCTION()
		void InputAimReleased();
	UFUNCTION()
		void InputWalkPressed();
	UFUNCTION()
		void InputWalkReleased();
	UFUNCTION()
		void InputSprintPressed();
	UFUNCTION()
		void InputSprintReleased();

	// Stamina Functions
	UFUNCTION()
		void SprintRunOn(float DeltaSeconds);
	UFUNCTION()
		void StaminaRestore(float DeltaSeconds);

	// Tick functions
	UFUNCTION()
		void FollowMouseCursorCharacterRotation(float DeltaTime);
	UFUNCTION()
		void StaminaLogic(float DeltaSeconds);

private:
	float AxisX = 0.f;
	float AxisY = 0.f;

	bool bIsIdleOrBackwards = true;
	bool bSprintAllowed = true;
	bool bIsStrafing = false;
};

